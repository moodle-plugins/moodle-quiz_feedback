<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'quiz_feedback', language 'fr'
 * @package   quiz_feedback
 * @copyright 2021 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Export feedback';

$string['correctness'] = 'Exactitude de la réponse';
$string['correctnessx'] = 'Exactitude {$a}';
$string['feedback'] = 'Export feedback';
$string['generalstudentfeedback'] = 'Retour général de l\'étudiant';
$string['includefields'] = 'Inclure les champs';
$string['incompatiblebehaviour'] = 'Ce rapport est compatible uniquement avec les comportements de question Retour de l\'étudiant.';
$string['responsex'] = 'Réponse {$a}';
$string['studentfeedback'] = 'Retour de l\'étudiant';
$string['studentfeedbackx'] = 'Retour de l\'étudiant {$a}';
