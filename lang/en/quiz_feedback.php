<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'quiz_feedback', language 'en'
 * @package   quiz_feedback
 * @copyright 2021 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Feedback export';

$string['correctness'] = 'Correctness';
$string['correctnessx'] = 'Correctness {$a}';
$string['feedback'] = 'Feedback export';
$string['feedbackfilename'] = 'feedback';
$string['feedbackreport'] = 'Feedback report';
$string['generalstudentfeedback'] = 'General student feedback';
$string['includefields'] = 'Include fields';
$string['incompatiblebehaviour'] = 'This report is only compatible with the Student feedback behaviours.';
$string['responsex'] = 'Response {$a}';
$string['studentfeedback'] = 'Student feedback';
$string['studentfeedbackx'] = 'Student feedback {$a}';

$string['privacy:preference:questiontext'] = 'Whether to show the question text columns in the report table.';
$string['privacy:preference:response'] = 'Whether to show the students\' response columns in the report table.';
$string['privacy:preference:correctness'] = 'Whether to show the correctness columns in the report table.';
$string['privacy:preference:studentfeedback'] = 'Whether to show the students\' feedback columns in the report table.';
